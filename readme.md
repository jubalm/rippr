# Rippr

A tool for scraping websites, creating a local copy of it for backup or modification

- generates a familiar folder structure for assets
- CDN assets will be downloaded; images, fonts, scripts, etc.
- beautify the output html and css (optional)

#### Requires

- [Node & NPM](https://nodejs.org)
- A browser

## Setup and first run

Just navigate to the root folder of this project and run the following command

```
npm install
```

Wait for the installation to complete before running the command after

```
npm run start
```

This will launch a node server and a localwebsite that's available over at [http://localhost:3000](http://localhost:3000).

## Usage

- Open up the website at [http://localhost:3000](http://localhost:3000) and provide the URL of the site to scrape/rip.
- A notification on the site will let you know once the target website has completed downloading all assets.
- The saved files will be located at [http://localhost:3000/sites](http://localhost:3000/sites) with the URL hash
