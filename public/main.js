// Serialize Object
(function() { $.fn.serializeObject = function() { var o = {}; var a = this.serializeArray(); $.each(a, function() { if (o[this.name]) { if (!o[this.name].push) { o[this.name] = [o[this.name]]; } o[this.name].push(this.value || ''); } else { o[this.name] = this.value || ''; } }); return o; }; })();

// Remove class that matches regex pattern
(function() { $.fn.removeClassMatch = function(pattern) { var regex = new RegExp("(^|\\s)" + pattern + "\\S+", "g"); return $(this).each(function() { $(this).removeClass (function (index, className) { return (className.match(regex) || []).join(' '); }); }); } })();

// Onload
(function($){
  $(function(){
    $('#rip').on('submit', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $input = $this.find('input[type="text"]');
      var $status = $this.find('.status');
      var $loader = $('<img/>').attr('src', 'loading.gif');
      var params = $this.serializeObject();

      // remove any `rip--*` classes
      $this.removeClassMatch('rip--')

      // add loading class
      $this.addClass('rip--loading');

      $.ajax({
        url: window.RIPPR_API,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(params)
      }).success(function(res) {

        if(res.errors) {
          res.errors.forEach( error => {
            console.log(error.message)
            $this.removeClassMatch('rip--').addClass('rip--error');
            $status.filter('.status--error').find('code').html(error.message)
          })
          return
        }

        $this.removeClassMatch('rip--').addClass('rip--success');
        $status.filter('.status--success').find('a').attr('href', res.url)
      }).fail(function(err) {
        console.log(err)
      })
    })
  });
})(jQuery)

