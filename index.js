const { readFileSync, writeFileSyc, readdirSync, statSync } = require('fs');
const path = require('path');
const url = require('url');
const express = require('express');
const port = process.env.PORT || 3000;
const server = express();
const bodyParser = require('body-parser');
const scrape = require('website-scraper');
const crypto = require('crypto');
const posthtml = require('posthtml');
const tidy = require('posthtml-tidy')

const rootPath = `./public`
const outputPath = `${rootPath}/sites`
const outputURL = `/sites`

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.get('/favicon.ico', function(req, res) {
  res.writeHead(200, { 'Content-Type': 'image/x-icon' });
  res.end();
});

server.use(express.static(path.resolve(__dirname, 'public')));

server.post('/rippr', function(req, res){
  const uri = url.parse(req.body.url).href
  const pathname = crypto.createHmac('sha1', 'rippr').update(uri).digest('hex')

  if(!req.body.url || !uri) {
    console.log(`error: invalid url`)
    return res.send({ errors: [{ status: 'invalid-url', message: 'invalid URL provided' }]})
  }

  const scrapeOptions = {
    urls: [req.body.url],
    directory: outputPath + `/${pathname}`,
    request: {
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2'
      }
    },
    subdirectories: [
      {directory: 'assets/images', extensions: ['.jpg', '.gif', '.png', '.svg']},
      {directory: 'assets/js', extensions: ['.js']},
      {directory: 'assets/css', extensions: ['.css']},
      {directory: 'assets/fonts', extensions: ['.eot', '.woff', '.woff2', '.ttf', 'otf']}
    ]
  };

  console.log(`info: started processing ${pathname}...`)

  scrape(scrapeOptions).then(results => {
    console.log(`info: scrape complete`)

    const localpath = path.resolve(outputPath, pathname, 'index.html')
    const source = readFileSync(localpath, 'utf-8')
    const dest = path.resolve(outputPath, pathname, 'index.html')

    return posthtml()
      .use(tidy())
      .process(source)
      .then( result => {
        console.log(`info: html processed`)
        return res.send({
          success: true,
          url: `${outputURL}/${pathname}`,
          path: path.resolve(outputPath, pathname)
        })
        writeFileSync(dest, result.html)
      })
  }).catch((err) => {
    return res.send({ errors: [{ status: 'internal', message: err.toString() }]})
    console.log(`error: unable to complete processing ${pathname}!`)
    console.log(err)
  });

});

server.get('/sites', function(req, res) {
  const dirs = p => readdirSync(p).filter(f => statSync(p+"/"+f).isDirectory())
  const links = dirs(outputPath).map((path) => {
    return `<p><a href="./${path}">` + path + '</a></p>'
  })
  return res.send(links.join(''))
})

server.listen(port, (err) => {
  if (err) { console.error(err); }
  console.info(`🌎 Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`);
});

